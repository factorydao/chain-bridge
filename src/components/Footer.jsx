import telegramIcon from "images/telegram.svg";
import mediumIcon from "images/medium.svg";
import twitterIcon from "images/twitter.svg";
import discordIcon from "images/discord.svg";
import styled from "styled-components";

const StyledAppFooter = styled.footer`
  background-color: #123863;
  color: #ffffff;
  font-size: 16.8px;
  padding-top: 21px;
  padding-bottom: 21px;
  font-family: WorkSans;
  position: absolute;
  bottom: 0;
  width: 100%;
`;

const StyledContainer = styled.div`
  max-width: 75%;
  margin-right: auto;
  margin-left: auto;

  @media (max-width: 768px) {
    max-width: 80%;
  }
`;

const StyledInnerContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  a {
    color: #00b1ff;
  }
`;

const StyledLinks = styled.div`
  * {
    margin-right: 24px;

    &:last-child {
      margin-right: 0;
    }
  }

  a {
    img {
      height: 21px;
    }
  }
`;

const Footer = () => {
  return (
    <StyledAppFooter>
      <StyledContainer>
        <StyledInnerContainer>
          <div>
            2021 All rights reserved &nbsp;
            <a href='https://finance.vote' target='_blank' rel='noreferrer'>
              finance.vote
            </a>
          </div>
          <StyledLinks>
            <a
              href='https://discord.com/invite/ggBT3mPd'
              target='_blank'
              rel='noreferrer'
            >
              <img src={discordIcon} alt='Discord' />
            </a>
            <a
              href='https://t.me/financedotvote'
              target='_blank'
              rel='noreferrer'
            >
              <img src={telegramIcon} alt='Telegram' />
            </a>
            &nbsp;
            <a
              href='https://twitter.com/financedotvote'
              target='_blank'
              rel='noreferrer'
            >
              <img src={twitterIcon} alt='Twitter' />
            </a>
            &nbsp;
            <a
              href='https://medium.com/@financedotvote'
              target='_blank'
              rel='noreferrer'
            >
              <img src={mediumIcon} alt='Medium' />
            </a>
          </StyledLinks>
        </StyledInnerContainer>
      </StyledContainer>
    </StyledAppFooter>
  );
};

export default Footer;
