import styled from "styled-components";

const StyledPlaceholder = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 0;
  padding: 9.8px 18.9px 9.7px 13px;
  font-family: "Work Sans";
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #efefef;
  img {
    margin-right: 10px;
  }
`;

const CustomPlaceholder = ({ data, innerProps, innerRef }) => {
  return data ? (
    <StyledPlaceholder ref={innerRef} {...innerProps}>
      <img src={data.icon} alt={data.label} />
      {data.label}
    </StyledPlaceholder>
  ) : null;
};

export default CustomPlaceholder;
