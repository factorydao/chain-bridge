import styled from "styled-components";

const Popup = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 10;
  left: 50%;
  top: 42%;
  transform: translate(-50%, -50%);
  width: 600px;
  height: 380px;
  flex-grow: 0;
  margin: 48px 249px 152px 16px;
  padding: 102px 94px 101px 95px;
  border: solid 2px #00b1ff;
  background-color: #0c2031;
`;

export default Popup;
