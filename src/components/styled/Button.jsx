import styled from "styled-components";

const Button = styled.button`
  padding: 15px 116.8px 14px 116.2px;
  background-color: rgb(0, 177, 255);
  color: #ffffff;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  border: none;

  &:disabled {
    background-color: rgba(0, 177, 255, 0.5);
  }
`;

export default Button;
