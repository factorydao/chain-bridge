import styled from "styled-components";

const StyledOptionDiv = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 0;
  margin: 15.7px 0 0;
  padding: 9.8px 18.9px 9.7px 13px;
  font-family: "Work Sans";
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #efefef;
  img {
    margin-right: 10px;
  }
  span {
    cursor: pointer;
  }
`;

const CustomSelectOption = ({ data, innerProps, innerRef }) => (
  <StyledOptionDiv ref={innerRef} {...innerProps}>
    <img src={data.icon} alt={data.label} />
    <span>{data.label}</span>
  </StyledOptionDiv>
);

export default CustomSelectOption;
