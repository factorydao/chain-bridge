import bscIcon from "images/bsc.png";
import ethIcon from "images/ethereum.png";
import polIcon from "images/polygon.png";

const chainsSelect = [
  { value: 0, label: "Ethereum", icon: ethIcon, chainId: 1 },
  { value: 2, label: "Polygon", icon: polIcon, chainId: 137 },
];
export default chainsSelect;
