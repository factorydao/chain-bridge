import { ethInstance, fromWei, sha3, toWei } from "evm-chain-scripts";
import web3 from "web3";
import Bridge from "../contracts/Bridge.json";
import ERC20Handler from "../contracts/ERC20Handler.json";
import Token from "../contracts/Token.json";

export async function getTokenBalance(id) {
  console.log('getTokenBalance', {id})
  const account = await ethInstance.getEthAccount();
  const token = await ethInstance.getContract("read", Token, {
    chainId: id,
  });
  const balance = await token.methods.balanceOf(account).call();
  return fromWei(balance);
}

export async function depositToBridge(amount, destinationBridgeId) {
  console.log('depositToBridge', {amount, destinationBridgeId})
  const account = await ethInstance.getEthAccount();
  const token = await ethInstance.getContract("write", Token);
  const handler = await ethInstance.getContract("write", ERC20Handler);
  const bridge = await ethInstance.getContract("write", Bridge);
  // this is universal across all chains, regardless of what it looks like
  const resourceId = sha3("ETH/BNB");
  const data = getDataArg(amount, account);
  console.log({ amount, destinationBridgeId, resourceId, data });

  const currentApproval = await token.methods
    .allowance(account, handler._address)
    .call();

  const needsApproval =
    parseFloat(fromWei(currentApproval)) < parseFloat(amount) + 10;
  const approvalAmount = "1000000000";

  if (needsApproval) {
    token.methods
      .approve(handler._address, toWei(approvalAmount))
      .send({ from: account, gas: 60000 });
  }

  return await bridge.methods
    .deposit(destinationBridgeId, resourceId, data)
    .send({ from: account, gas: "300000" });
}

function getDataArg(depositAmount, recipientAddress) {
  const data =
    web3.utils.leftPad(
      web3.utils.numberToHex(web3.utils.toWei(depositAmount)),
      64
    ) +
    web3.utils.leftPad(web3.utils.numberToHex(20), 64).substr(2) +
    recipientAddress.substr(2);

  return data;
}

//destinationChainId = 1; resourceId = web3.utils.sha3('ETH/BNB'); depositAmount = '1'; recipientAddress = accounts[0];
//data = web3.utils.leftPad(web3.utils.numberToHex(web3.utils.toWei(depositAmount)), 64) + web3.utils.leftPad(web3.utils.numberToHex(20), 64).substr(2) + recipientAddress.substr(2)
//bridge.deposit(destinationChainId, resourceId, data, {from: accounts[0], gas: '300000', gasPrice: '10000000000'})
