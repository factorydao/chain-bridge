// this function detects most providers injected at window.ethereum
import detectEthereumProvider from '@metamask/detect-provider'
import Web3 from 'web3'

// const { REACT_APP_ETH_PROVIDER_URL } = process.env
// const REACT_APP_ETH_PROVIDER_URL = 'ws://localhost:8545'
const REACT_APP_ETH_PROVIDER_URL = 'wss://mainnet.infura.io/ws/v3/044bfbb71eeb4452a66feb7768d7a1b8'
    , ethereum = window.ethereum

// console.log({ REACT_APP_ETH_PROVIDER_URL })

if (!ethereum) {
  console.warn('Missing ethereum object in the global scope')
}

const truffleContract = require('@truffle/contract')
    , readWeb3 = new Web3(getProvider(REACT_APP_ETH_PROVIDER_URL))

let writeWeb3
  , currentAccount = null

console.log('Using web3 version: ', readWeb3.version)

window.addEventListener('load', async () => {
  const provider = await detectEthereumProvider()

  if (provider !== ethereum) {
    console.error('Do you have multiple wallets installed?')
  }

  // From now on, this should always be true:
  // provider === ethereum
  if (provider) {
    await connectWallet()

    writeWeb3 = new Web3(provider)

    dispatchEvent(EVENTS.NETWORK_CHANGE, writeWeb3)
  } else {
    writeWeb3 = readWeb3

    dispatchEvent(EVENTS.NETWORK_CHANGE, writeWeb3)
  }
})

const listeners = {}
    , EVENTS = {
      NETWORK_CHANGE: 'networkChange'
    }

function dispatchEvent (eventName, ...args) {
  const handlers = listeners[eventName]

  if (handlers) {
    handlers.forEach(handler => handler(...args))
  }
}

function addEventListener (eventName, handler) {
  let handlers = listeners[eventName]

  if (!handlers) {
    handlers = listeners[eventName] = []
  }

  handlers.push(handler)
}

function removeEventListener (eventName, handler) {
  const handlers = listeners[eventName]

  if (handlers) {
    const index = handlers.indexOf(handler)

    handlers.splice(index, 1)
  }
}

export default {
  EVENTS,
  addEventListener,
  removeEventListener
}

export async function metamaskWrongNetwork () {
  const expected = await readWeb3?.eth?.net?.getId()
      , current = await writeWeb3?.eth?.net?.getId()

  return {
    isOk: expected === current,
    expected,
    current
  }
}

// For now, 'eth_accounts' will continue to always return an array
function handleAccountsChanged (accounts) {
  if (accounts.length === 0) {
    // MetaMask is locked or the user has not connected any accounts
    currentAccount = null
  } else if (accounts[0] !== currentAccount) {
    currentAccount = accounts[0]
    // Do any other work!
  }
}

export async function connectWallet () {
  await ethereum.request({ method: 'eth_requestAccounts' })
    .then(handleAccountsChanged)
    .catch((err) => {
      if (err.code === 4001) {
        // EIP-1193 userRejectedRequest error
        // If this happens, the user rejected the connection request.
        console.log('Please connect to MetaMask.')
      } else {
        console.error(err)
      }
    })
}

export function isWalletConnected () {
  return currentAccount != null
}

export async function getEthAccount (index = 0) {
  if (!currentAccount) {
    await connectWallet()
    if (!currentAccount) {
      const msg = 'Please ensure that metamask or another browser wallet is installed and activated to use this feature'

      console.error(msg)
      alert(msg)
      throw msg
    }
  }
  return currentAccount
}

/**********************************************************/
/* Handle chain (network) and chainChanged (per EIP-1193) */
/**********************************************************/

// Normally, we would recommend the 'eth_chainId' RPC method, but it currently
// returns incorrectly formatted chain ID values.
// const currentChainId = ethereum?.chainId

ethereum?.on('chainChanged', handleChainChanged)
ethereum?.on('accountsChanged', handleChainChanged)

function handleChainChanged (_chainId) {
  // We recommend reloading the page, unless you must do otherwise
  window.location.reload()
}

const contracts = { read: {}, write: {} }

export async function getContract (platform, contract) {
  if (!contracts[platform][contract.contractName]) {
    if (platform === 'write') {
      const t = truffleContract(contract)

      t.setProvider(writeWeb3.currentProvider)
      contracts[platform][contract.contractName] = await t.deployed()
    } else if (platform === 'read') {
      const t = truffleContract(contract)

      t.setProvider(readWeb3.currentProvider)
      contracts[platform][contract.contractName] = await t.deployed()
      // console.log(contract.contractName, contracts[platform][contract.contractName])
    }
  }
  const c = contracts[platform][contract.contractName]
  //  console.log('acquiring contract', contract.contractName, 'at address', c.address)

  return c
}

export async function isUserManagement (contract) {
  let isManagement = false

  if (writeWeb3) {
    const accounts = await writeWeb3.eth.getAccounts()
        , metamaskAddr = accounts[0]
        , managementAddr = await getManagementAddress(contract)

    isManagement = metamaskAddr === managementAddr
  }
  return isManagement
}

async function getManagementAddress (contract) {
  const c = await getContract('read', contract)
      , mgmt = await c.management()

  return mgmt
}

export function getProvider (url) {
  const provider = new Web3.providers.WebsocketProvider(url)

  return provider
}

export async function getAllEvents (
  callback,
  contractAbi,
  eventName,
  filter,
  fromBlock = 0,
  toBlock = 'latest',
  eventSignature,
  eventInputs
) {
  const readContract = await getWeb3Contract(contractAbi)
      , truffleContract = await getContract('read', contractAbi)

  readContract.getPastEvents(eventName, {
    filter: filter,
    fromBlock: fromBlock,
    toBlock: toBlock
  }, (error, events) => {
    if (error) {
      console.error(error)
      return
    }

    for (var i = 0; i < events.length; i++) {
      const event = events[i]

      event.returnValues.txHash = event.transactionHash
      callback(event.returnValues)
    }
  })
  subscribeLogEvent(truffleContract, eventName, eventSignature, eventInputs, (event) => {
    callback(event)
  })
}

export function subscribeLogEvent (contract, eventName, eventSig, eventInputs, callback) {
  readWeb3.eth.subscribe('logs', {
    address: contract.address,
    topics: [ eventSig ]
  }, (error, result) => {
    if (!error) {
      console.log('result', result)
      const eventObj = readWeb3.eth.abi.decodeLog(
        eventInputs,
        result.data,
        result.topics.slice(1)
      )

      eventObj.txHash = result.transactionHash
      callback(eventObj)
    } else {
      console.error(`Error making subscription to ${eventName} on ${contract.address}: ${error}`)
    }
  })
}

export async function getWeb3Contract (contractAbi) {
  const networkId = await getNetworkId()
      , contractAddress = contractAbi.networks[networkId].address
      , readContract = new readWeb3.eth.Contract(contractAbi.abi, contractAddress)

  return readContract
}

export async function getReadContractByAddress (contract, address) {
  const t = truffleContract(contract)

  t.setProvider(readWeb3.currentProvider)
  const readContract = await t.at(address)

  return readContract
}

export function bytesToString (bytes) {
  return Web3.utils.hexToString(bytes)
}

export function toChecksumAddress (address) {
  return Web3.utils.toChecksumAddress(address)
}

export function fromWei (num) {
  return Web3.utils.fromWei(num)
}

export function toWei (num) {
  return Web3.utils.toWei(num)
}

export function toBN (num) {
  return Web3.utils.toBN(num)
}

export function sha3 (arg) {
  return Web3.utils.sha3(arg)
}

export async function getNetworkId () {
  return await readWeb3.eth.net.getId()
}
